import React from 'react'
import './App.scss';
import Modal from "./components/Modal";

function App() {
  return (
      <div className='modals'>
          <Modal
                 buttonsDesign = 'buttonsDesign'
                 btnOpenModals = 'btnOpenModals'
                 modalBtnAll = 'modalBtnAll'

                 textBtn = 'Open First Modal'
                 headerText = 'Do you want to delete this file?'
                 text ={
                     <div>
                         <p>it wont possible to undo this action.</p>
                         <p>Are you sure you want to delete it?</p>
                     </div>
                 }
                 btnOk = 'Ok'
                 btnCancel = 'Cancel'
                 closeButton={true}
                 className='modalBtn1'

                 modalHeader='modalHeader'
                 modalCenter='modalCenter'
          />
          <Modal
                  buttonsDesign = 'buttonsDesign'
                  btnOpenModals = 'btnOpenModals'
                  modalBtnAll = 'modalBtnAll'

                  textBtn='Open Second Modal'
                  headerText='Do you want to add this file? cross here ---->'
                  text ={
                      <div>
                          <p>Once you delete this file, it wont possible to undo this action.</p>
                          <p>Are you sure?</p>
                      </div>
                  }
                  btnOk = 'Ok'
                  closeButton={false}
                  btnCancel = 'Cancel'
                  className='modalBtn2'

                  modalHeader='modalHeader2'
                  modalCenter='modalCenter2'
          />
      </div>
  );
}

export default App;
