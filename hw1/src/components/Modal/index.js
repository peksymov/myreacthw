import React, {Component} from 'react'
import './Modal.scss'
import Button from "../Buttons";


class Modal extends Component {
    constructor(props) {
        super(props);
        this.handleModal = this.handleModal.bind(this)    // Первый способ
    }

    state = {
        isOpen: false,
    }
    handleModal () {
        this.setState({isOpen: !this.state.isOpen})
    }

    render() {

        return (
            <div>
                <Button className={`${this.props.buttonsDesign} ${this.props.btnOpenModals}`} text={this.props.textBtn} onClick={this.handleModal.bind(this)}/>   {/*// Второй способ*/}
                {this.state.isOpen &&
                <div className='modal' onClick={this.handleModal.bind(this)}>    {/*// Второй способ*/}
                    <div className='modal-body' onClick={e => e.stopPropagation()} >
                        <div className={this.props.modalHeader}>
                            <span className='spanChange'>{this.props.headerText}</span>
                            {this.props.closeButton &&
                            <span className='closeBtn' onClick={()=>{this.handleModal()}}>&times;</span>   // Третий способ
                            }

                        </div>
                        <div className={this.props.modalCenter}>
                            {this.props.text}
                            <div className='buttons-Ok-Cancel '>
                                <Button className={`${this.props.buttonsDesign} ${this.props.className} ${this.props.modalBtnAll}`} text={this.props.btnOk}/>
                                <Button className={`${this.props.buttonsDesign} ${this.props.className} ${this.props.modalBtnAll}`} text={this.props.btnCancel} onClick={this.handleModal}/>  {/*// Первый способ*/}
                            </div>
                        </div>
                    </div>
                </div>}
            </div>
        );
    }
}
export default Modal