import React from 'react'
import './Button.scss'

class Button extends React.Component{

    render() {
        console.log(this.props)
        return(
            <button className={`${this.props.className} ${this.props.modalBtnAll}`} onClick={this.props.onClick} >{this.props.text}</button>
        )
    }
}
export default Button
